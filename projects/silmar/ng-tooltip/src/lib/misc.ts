import { ConnectedPosition } from '@angular/cdk/overlay';

const CSS_WRAP     = 'si-tl-wrap';
const CSS_TOP      = 'si-top';
const CSS_TOP_S    = 'si-top-start';
const CSS_TOP_E    = 'si-top-end';
const CSS_BOTTOM   = 'si-bottom';
const CSS_BOTTOM_S = 'si-bottom-start';
const CSS_BOTTOM_E = 'si-bottom-end';
const CSS_LEFT     = 'si-left';
const CSS_RIGHT    = 'si-right';

export const POS_TOP_C: ConnectedPosition    = {
  originX    : 'center',
  originY    : 'top',
  overlayX   : 'center',
  overlayY   : 'bottom',
  offsetY    : -7,
  panelClass : [ CSS_TOP, CSS_WRAP ]
};
export const POS_TOP_S: ConnectedPosition    = {
  originX    : 'start',
  originY    : 'top',
  overlayX   : 'start',
  overlayY   : 'bottom',
  offsetY    : -7,
  panelClass : [ CSS_TOP, CSS_TOP_S, CSS_WRAP ]
};
export const POS_TOP_E: ConnectedPosition    = {
  originX    : 'end',
  originY    : 'top',
  overlayX   : 'end',
  overlayY   : 'bottom',
  offsetY    : -7,
  panelClass : [ CSS_TOP, CSS_TOP_E, CSS_WRAP ]
};
export const POS_BOTTOM_C: ConnectedPosition = {
  originX    : 'center',
  originY    : 'bottom',
  overlayX   : 'center',
  overlayY   : 'top',
  offsetY    : 6,
  panelClass : [ CSS_BOTTOM, CSS_WRAP ]
};
export const POS_BOTTOM_S: ConnectedPosition = {
  originX    : 'start',
  originY    : 'bottom',
  overlayX   : 'start',
  overlayY   : 'top',
  offsetY    : 6,
  panelClass : [ CSS_BOTTOM, CSS_BOTTOM_S, CSS_WRAP ]
};
export const POS_BOTTOM_E: ConnectedPosition = {
  originX    : 'end',
  originY    : 'bottom',
  overlayX   : 'end',
  overlayY   : 'top',
  offsetY    : 6,
  panelClass : [ CSS_BOTTOM, CSS_BOTTOM_E, CSS_WRAP ]
};
export const POS_LEFT_C: ConnectedPosition   = {
  originX    : 'start',
  originY    : 'center',
  overlayX   : 'end',
  overlayY   : 'center',
  offsetX    : -7,
  panelClass : [ CSS_LEFT, CSS_WRAP ]
};
export const POS_RIGHT_C: ConnectedPosition  = {
  originX    : 'end',
  originY    : 'center',
  overlayX   : 'start',
  overlayY   : 'center',
  offsetX    : 6,
  panelClass : [ CSS_RIGHT, CSS_WRAP ]
};
